package com.afs.restapi.service;

import com.afs.restapi.entity.Company;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.repository.CompanyJPARepository;
import com.afs.restapi.repository.EmployeeJPARepository;
import com.afs.restapi.repository.InMemoryCompanyRepository;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.InMemoryEmployeeRepository;
import com.afs.restapi.service.dto.CompanyRequest;
import com.afs.restapi.service.dto.CompanyResponse;
import com.afs.restapi.service.dto.EmployeeResponse;
import com.afs.restapi.service.mapper.CompanyMapper;
import com.afs.restapi.service.mapper.EmployeeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CompanyService {

    private final InMemoryCompanyRepository inMemoryCompanyRepository;
    private final InMemoryEmployeeRepository inMemoryEmployeeRepository;

    private final CompanyJPARepository companyJPARepository;
    private final EmployeeJPARepository employeeJPARepository;

    @Autowired
    private CompanyMapper mapper;
    @Autowired
    private EmployeeMapper employeeMapper;

    public CompanyService(InMemoryCompanyRepository inMemoryCompanyRepository, InMemoryEmployeeRepository inMemoryEmployeeRepository, CompanyJPARepository companyJPARepository, EmployeeJPARepository employeeJPARepository) {
        this.inMemoryCompanyRepository = inMemoryCompanyRepository;
        this.inMemoryEmployeeRepository = inMemoryEmployeeRepository;
        this.companyJPARepository = companyJPARepository;
        this.employeeJPARepository = employeeJPARepository;
    }

    public InMemoryCompanyRepository getCompanyRepository() {
        return inMemoryCompanyRepository;
    }

    public InMemoryEmployeeRepository getEmployeeRepository() {
        return inMemoryEmployeeRepository;
    }

    public List<CompanyResponse> findAll() {
        List<Company> companies = companyJPARepository.findAll();
        return toResponseList(companies);
    }
    public List<CompanyResponse> toResponseList(List<Company> companies){
        List<CompanyResponse> companyResponses = new ArrayList<>();
        companies.stream().forEach(company -> {
            CompanyResponse companyResponse = mapper.toResponse(company);
            companyResponse.setEmployeesCount(findEmployeesByCompanyId(company.getId()).size());
            companyResponses.add(companyResponse);
        });
        return companyResponses;
    }
    public List<CompanyResponse> findByPage(Integer page, Integer size) {
        List<Company> companies = getCompanyRepository().findByPage(page, size);
        return toResponseList(companies);
    }

    public CompanyResponse findById(Long id) {
        Company company = companyJPARepository.findById(id).orElseThrow(CompanyNotFoundException::new);
        List<Employee> employees = employeeJPARepository.findByCompanyId(company.getId());
        company.setEmployees(employees);
        CompanyResponse response = mapper.toResponse(company);
        response.setEmployeesCount(employees.size());
        return response;
    }

    public CompanyResponse update(Long id, Company company) {
        Optional<Company> optionalCompany = companyJPARepository.findById(id);
        optionalCompany.ifPresent(previousCompany -> previousCompany.setName(company.getName()));
        companyJPARepository.save(optionalCompany.get());
        return findById(optionalCompany.get().getId());
    }

    public CompanyResponse create(CompanyRequest companyRequest) {
        CompanyResponse companyResponse = mapper.toResponse(companyJPARepository.save(mapper.toEntity(companyRequest)));
        companyResponse.setEmployeesCount(findEmployeesByCompanyId(companyResponse.getId()).size());
        return  companyResponse;
    }

    public List<EmployeeResponse> findEmployeesByCompanyId(Long id) {
        List<Employee> employees = employeeJPARepository.findByCompanyId(id);
        List<EmployeeResponse> employeeResponses = new ArrayList<>();
        employees.stream().forEach(employee -> {
            employeeResponses.add(employeeMapper.toResponse(employee));
        });
        return employeeResponses;
    }

    public void delete(Long id) {
        companyJPARepository.deleteById(id);
    }
}
