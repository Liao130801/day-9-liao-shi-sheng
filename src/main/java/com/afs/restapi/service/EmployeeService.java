package com.afs.restapi.service;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.repository.EmployeeJPARepository;
import com.afs.restapi.repository.InMemoryEmployeeRepository;
import com.afs.restapi.service.dto.EmployeeRequest;
import com.afs.restapi.service.dto.EmployeeResponse;
import com.afs.restapi.service.mapper.EmployeeMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeService {

    private final InMemoryEmployeeRepository inMemoryEmployeeRepository;
    private  final EmployeeJPARepository employeeJPARepository;
    @Autowired
    private EmployeeMapper mapper;
    public EmployeeService(InMemoryEmployeeRepository inMemoryEmployeeRepository, EmployeeJPARepository employeeJPARepository) {
        this.inMemoryEmployeeRepository = inMemoryEmployeeRepository;
        this.employeeJPARepository = employeeJPARepository;
    }

    public InMemoryEmployeeRepository getEmployeeRepository() {
        return inMemoryEmployeeRepository;
    }

    public EmployeeJPARepository getEmployeeJPARepository(){
        return employeeJPARepository;
    }

    public List<EmployeeResponse> findAll() {
         List<EmployeeResponse> employeeResponses = new ArrayList<>();
        List<Employee>  employees = getEmployeeJPARepository().findAll();
        employees.stream().forEach(employee -> {
            employeeResponses.add(mapper.toResponse(employee));
        });
        return employeeResponses;
    }

    public Employee findById(Long id) {
        return getEmployeeJPARepository().findById(id)
                .orElseThrow(EmployeeNotFoundException::new);
    }

    public EmployeeResponse update(Long id, EmployeeRequest employeeRequest) {
        Employee toBeUpdatedEmployee = findById(id);
        if (employeeRequest.getSalary() != null) {
            toBeUpdatedEmployee.setSalary(employeeRequest.getSalary());
        }
        if (employeeRequest.getAge() != null) {
            toBeUpdatedEmployee.setAge(employeeRequest.getAge());
        }
        getEmployeeJPARepository().save(toBeUpdatedEmployee);
        return mapper.toResponse(findById(id));
    }

    public List<EmployeeResponse> findAllByGender(String gender) {
        List<EmployeeResponse> employeeResponses = new ArrayList<>();
        List<Employee>  employees = employeeJPARepository.findByGender(gender);
        employees.stream().forEach(employee -> {
            employeeResponses.add(mapper.toResponse(employee));
        });
        return employeeResponses;
    }

    public EmployeeResponse create(EmployeeRequest employeeRequest) {
        return mapper.toResponse(employeeJPARepository.save(mapper.toEntity(employeeRequest)));
    }

    public List<EmployeeResponse> findByPage(Integer page, Integer size) {
        List<EmployeeResponse> employeeResponses = new ArrayList<>();
        List<Employee>  employees = employeeJPARepository.findAll(Pageable.ofSize(size).withPage(page-1)).stream().collect(Collectors.toList());
        employees.stream().forEach(employee -> {
            employeeResponses.add(mapper.toResponse(employee));
        });
        return employeeResponses;
    }

    public void delete(Long id) {
        employeeJPARepository.deleteById(id);
    }
}
